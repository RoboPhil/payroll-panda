/**
 * Product.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const fx = require('money')
const fetch = require('node-fetch')

const defaultCurrency = 'USD'
const fetchRates = () => {
  fetch('https://api.fixer.io/latest?base=USD')
  .then((resp) => resp.json())
  .then(cacheRates)
}
const cacheRates = data => {
  fx.rates = data.rates
  fx.base = data.base
  fx.date = data.date
}
const updateCurrency = (currency, modelObj) => {
  modelObj.cost = `${currency} ${modelObj.cost}`
  modelObj.price = `${currency} ${modelObj.price}`

  return modelObj
}

// fetch currency rates
fetchRates()

module.exports = {

  attributes: {
    description: {
      type: 'string',
      required: true
    },
    cost: {
      type: 'integer',
      required: true
    },
    price: {
      type: 'integer',
      required: true
    },
    stock: {
      type: 'integer',
      defaultsTo: 0,
      required: true
    },
    toJSON: function () {
      const modelObj = this.toObject()
      return updateCurrency(defaultCurrency, modelObj)
    },
    toCurrency: function (currency) {
      const modelObj = this.toObject()
      // if rate is not available, return in dollars

      if (!fx.rates[currency]) return updateCurrency(defaultCurrency, modelObj)
      modelObj.cost = fx(modelObj.cost).from('USD').to(currency)
      modelObj.price = fx(modelObj.price).from('USD').to(currency)

      return updateCurrency(currency, modelObj)
    },
    convert: function (currency, value) {
      if (!fx.rates[currency]) return `${defaultCurrency} ${value}`

      const newValue = fx(value).from('USD').to(currency)
      return `${currency} ${newValue}`
    }
  }
}
