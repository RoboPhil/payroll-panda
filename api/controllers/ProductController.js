/**
 * ProductController
 *
 * @description :: Server-side logic for managing products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
/* global Product */

module.exports = {
  find: function (req, res) {
    const { currency } = req.query

    Product.find()
    .then(products => {
      if (!currency) {
        return res.json(products)
      }

      const newProducts = products.map(product => product.toCurrency(currency))
      return res.json(newProducts)
    })
    .catch(error => {
      res.negotiate(error)
    })
  },
  findOne: function (req, res) {
    const { currency } = req.query
    const { id } = req.params

    Product.findOne({id})
    .then(product => {
      if (!product) {
        return res.notFound()
      }
      if (!currency) {
        return res.json(product)
      }

      const newProduct = product.toCurrency(currency)
      return res.json(newProduct)
    })
    .catch(error => {
      res.negotiate(error)
    })
  },
  cost: function (req, res) {
    const { currency } = req.query

    Product.find()
    .then(products => {
      const total = products.reduce((sum, product) => sum + (product.cost * product.stock), 0)
      if (products.length === 0) {
        return res.json(total)
      }

      return res.json(products[0].convert(currency, total))
    })
    .catch(error => {
      return res.negotiate(error)
    })
  },
  costOne: function (req, res) {
    const { id } = req.params
    const { currency } = req.query

    Product.findOne({id})
    .then(product => {
      if (!product) {
        return res.notFound()
      }

      const total = product.cost * product.stock
      return res.json(product.convert(currency, total))
    })
    .catch(error => {
      res.negotiate(error)
    })
  },
  price: function (req, res) {
    const { currency } = req.query

    Product.find()
    .then(products => {
      const total = products.reduce((sum, product) => sum + (product.price * product.stock), 0)
      if (products.length === 0) {
        return res.json(total)
      }

      return res.json(products[0].convert(currency, total))
    })
    .catch(error => {
      return res.negotiate(error)
    })
  },
  priceOne: function (req, res) {
    const { id } = req.params
    const { currency } = req.query

    Product.findOne({id})
    .then(product => {
      if (!product) {
        return res.notFound()
      }

      const total = product.price * product.stock
      return res.json(product.convert(currency, total))
    })
    .catch(error => {
      res.negotiate(error)
    })
  }
}
